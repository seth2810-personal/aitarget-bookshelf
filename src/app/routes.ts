import { Routes } from '@angular/router';

import { BookAddComponent } from './book/book-add.component';
import { BookListComponent } from './book/book-list.component';
import { BookViewComponent } from './book/book-view.component';

export const routes: Routes = [
  {
    path: 'books/add',
    component: BookAddComponent
  },
  {
    path: 'books/:id',
    component: BookViewComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    component: BookListComponent
  },
  {
    path: '**',
    redirectTo: '',
  }
];
