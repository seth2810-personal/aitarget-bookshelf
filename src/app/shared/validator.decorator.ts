import { ValidatorFn, ValidationErrors, FormControl } from '@angular/forms';

export function ValidatorDecorator(name: string, validator: (value: any) => boolean): ValidatorFn {
  return (control: FormControl): ValidationErrors | null => {
    const { value } = control;

    if (!value || validator(value)) {
      return null;
    }

    return { [name]: { value } };
  };
}
