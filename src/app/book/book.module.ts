import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { RatingModule } from 'primeng/rating';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MessageModule } from 'primeng/message';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BookService } from './book.service';
import { BookAddComponent } from './book-add.component';
import { BookListComponent } from './book-list.component';
import { BookViewComponent } from './book-view.component';
import { BookFormComponent } from './book-form.component';
import { BookCoverComponent } from './book-cover.component';

@NgModule({
  imports: [
    FormsModule,
    RouterModule,
    CommonModule,
    ButtonModule,
    RatingModule,
    MessageModule,
    InputTextModule,
    InputTextareaModule,
    ReactiveFormsModule,
  ],
  declarations: [
    BookAddComponent,
    BookViewComponent,
    BookListComponent,
    BookFormComponent,
    BookCoverComponent,
  ],
  providers: [
    BookService
  ]
})
export class BookModule { }
