import { Component, ViewChild, ElementRef, AfterViewInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-book-cover',
  templateUrl: './book-cover.component.html'
})
export class BookCoverComponent implements AfterViewInit {
  @Input() public readonly src: string;
  @Input() public readonly alt: string;
  @ViewChild('img') private readonly imgEl: ElementRef;

  ngAfterViewInit() {
    this.imgEl.nativeElement.onerror = () => {
      this.imgEl.nativeElement.src = this.alt;
    };
  }
}
