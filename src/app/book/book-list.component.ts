import { Component, OnInit } from '@angular/core';

import { Book } from './book.interface';
import { BookService } from './book.service';

@Component({
  styleUrls: ['./book-list.component.scss'],
  templateUrl: './book-list.component.html'
})
export class BookListComponent implements OnInit {
  public books: Book[];

  constructor(
    private readonly bookService: BookService
  ) { }

  ngOnInit() {
    this.books = this.bookService.list();
  }
}
