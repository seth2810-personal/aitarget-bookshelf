import * as validator from 'validator';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ValidatorDecorator } from '../shared/validator.decorator';

@Component({
  selector: 'app-book-form',
  styleUrls: ['./book-form.component.scss'],
  templateUrl: './book-form.component.html'
})
export class BookFormComponent {
  public readonly form: FormGroup = new FormGroup({
    title: new FormControl('', [ Validators.required ]),
    cover: new FormControl('', [ ValidatorDecorator('url', validator.isURL) ]),
    description: new FormControl('', [ Validators.required ]),
    author: new FormControl('', [ Validators.required ]),
    isbn: new FormControl('', [ ValidatorDecorator('isbn', validator.isISBN) ]),
    year: new FormControl('', [ Validators.required, Validators.min(1970), Validators.max((new Date()).getFullYear()) ]),
    rating: new FormControl('', [ Validators.min(0), Validators.max(5) ]),
  });
}
