export interface Book {
  title: string;
  cover?: string;
  description: string;
  author: string;
  isbn: string;
  year: number;
  rating?: number;
}
