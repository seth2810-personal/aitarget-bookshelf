import { Router } from '@angular/router';
import { Component, ViewChild, Input } from '@angular/core';

import { BookService } from './book.service';
import { BookFormComponent } from './book-form.component';

@Component({
  templateUrl: './book-add.component.html'
})
export class BookAddComponent {
  @ViewChild(BookFormComponent)
  public readonly bookForm: BookFormComponent;

  constructor(
    private readonly router: Router,
    private readonly bookService: BookService
  ) { }

  addBook() {
    this.bookService.create(this.bookForm.form.value);

    this.router.navigateByUrl('');
  }
}
