import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';

import { Book } from './book.interface';
import { BookService } from './book.service';
import { BookFormComponent } from './book-form.component';

@Component({
  styleUrls: ['./book-view.component.scss'],
  templateUrl: './book-view.component.html'
})
export class BookViewComponent implements OnInit {
  @ViewChild(BookFormComponent)
  public readonly bookForm: BookFormComponent;

  public edit = false;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly bookService: BookService,
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const book: Book = this.bookService.read(+id);

    if (!book) {
      return this.router.navigateByUrl('');
    }

    this.bookForm.form.setValue(book);
    this.bookForm.form.disable();
  }

  editBook() {
    this.bookForm.form.enable();
  }

  updateBook() {
    const id = this.route.snapshot.paramMap.get('id');
    const book: Book = this.bookForm.form.value;

    this.bookForm.form.markAsPristine();
    this.bookService.update(+id, book);
    this.bookForm.form.setValue(book);
    this.bookForm.form.disable();
  }
}
