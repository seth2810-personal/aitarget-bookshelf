import { Injectable, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';

import { Book } from './book.interface';
import BOOKS from './books.json';

const STORAGE_KEY = 'books';

@Injectable()
export class BookService {
  private books: Book[];

  constructor(
    @Inject(SESSION_STORAGE) private storage: StorageService
  ) {
    if (!this.storage.get(STORAGE_KEY)) {
      this.storage.set(STORAGE_KEY, BOOKS);
    }

    this.books = this.storage.get(STORAGE_KEY);
  }

  list(): Book[] {
    return this.books;
  }

  create(book: Book): void {
    this.books.push(book);
    this.storage.set(STORAGE_KEY, this.books);
  }

  read(id: number): Book {
    return this.books[id];
  }

  update(id: number, book: Book) {
    this.books[id] = book;
    this.storage.set(STORAGE_KEY, this.books);
  }
}
