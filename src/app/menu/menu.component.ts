import { MenuItem } from 'primeng/api';
import { Component } from '@angular/core';

@Component({
  selector: 'app-menu',
  styleUrls: ['./menu.component.scss'],
  templateUrl: './menu.component.html'
})
export class MenuComponent {
  public readonly menu: MenuItem[] = [
    {
      label: 'Главная',
      routerLink: ['/']
    },
    {
      label: 'Добавить книгу',
      routerLink: ['/books/add']
    }
  ];
}
