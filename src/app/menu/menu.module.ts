import { NgModule } from '@angular/core';
import { MenuModule as PrimeMenuModule } from 'primeng/menu';

import { MenuComponent } from './menu.component';

@NgModule({
  imports: [
    PrimeMenuModule
  ],
  declarations: [
    MenuComponent
  ],
  exports: [
    MenuComponent
  ]
})
export class MenuModule { }
