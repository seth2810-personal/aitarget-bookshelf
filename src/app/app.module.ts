import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MenuModule } from './menu/menu.module';
import { BookModule } from './book/book.module';

import { routes } from './routes';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    BrowserModule,
    MenuModule,
    BookModule
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
